'use strict';

const fs = require('fs');
const path = require('path');
const file = fs.readFileSync('./.babelrc');
const babelConfig = JSON.parse(file);

const APP_DIR = path.resolve(__dirname, 'src/_assets/js');

module.exports = {
  entry: {
    app: APP_DIR + '/app.js'
  },
  output: {
    path: './src/assets/js',
    filename: 'app.js'
  },
  module: {
    preLoaders: [
      {
        test: /\.jsx?$/,
        loader: 'babel',
        query: babelConfig,
        include: APP_DIR
      },
      {
        test: /\.json$/,
        loader: 'json'
      }
    ],
    loaders: []
  },
  externals: {
    react: 'React',
    'react-dom': 'ReactDOM'
  },
  resolve: {
    extensions: ['', '.js', '.jsx']
  }
};
