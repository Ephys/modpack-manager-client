import config from './config';
import { ApiError } from './Error';

let fetchHeaders = new Headers({ 'Content-Type': 'application/json' });

const API = {

  // TODO error handling
  call(path, method, data) {

    return fetch(config.API_PATH + path, {
      method,
      body: JSON.stringify(data),
      headers: fetchHeaders
    }).catch(e => {
      throw new ApiError('ERR_NETWORK', e, 'Network error.');
    }).then(raw => {
      return raw.json().then(response => {
        if (!raw.ok) {
          throw new ApiError(response.message, response, `Error ${raw.status}.`);
        }

        return response;
      });
    });
  },

  setAuthToken(token) {
    if (token) {
      fetchHeaders.set('Authorization', `JWT ${token}`);
      localStorage.setItem('Authorization', token);
    } else {
      fetchHeaders.delete('Authorization');
      localStorage.removeItem('Authorization');
    }
  },

  isAuthentified() {
    return !!localStorage.getItem('Authorization');
  }
};

const token = localStorage.getItem('Authorization');
if (token) {
  API.setAuthToken(token);
}

export default API;
