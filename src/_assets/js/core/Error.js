
export class ApiError {
  constructor(errorCode, data, message = '') {
    this.data = data;
    this.errorCode = errorCode;
    this.message = message;
  }
}
