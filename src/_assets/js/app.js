import ReactDom from 'react-dom';
import { Router, Route, hashHistory } from 'react-router';
import LoginPage, { path as loginPath } from './dom/pages/login';
import IndexPage, { path as indexPath } from './dom/pages/index';
import api from './core/api';

function requireAuth(nextState, replace) {

  if (!api.isAuthentified()) {
    replace({
      pathname: loginPath,
      state: { nextPathname: nextState.location.pathname }
    });
  }
}

function denyAuth(nextState, replace) {

  if (api.isAuthentified()) {
    replace({
      pathname: indexPath,
      state: { nextPathname: nextState.location.pathname }
    });
  }
}

function logout(nextState, replace) {
  api.setAuthToken('');

  requireAuth(nextState, replace);
}

ReactDom.render(
  <Router history={hashHistory}>
    <Route path={ indexPath } component={IndexPage} onEnter={requireAuth} />
    <Route path={ loginPath } component={LoginPage} onEnter={denyAuth}/>
    <Route path="/logout" onEnter={logout}/>
  </Router>,
  document.querySelector('#modpack_manager_app')
);
