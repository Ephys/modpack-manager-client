import React, { PropTypes } from 'react';
import classnames from 'classnames';

function _onChange(input, validator, component) {
  if (input.value.length === 0) {
    component.setValidity(true);
    component.setState({ active: false });
    return;
  }

  component.setState({ active: true });

  const validity = validator(input.value);
  component.setValidity(validity.valid, validity.message);
}

function _onInvalid(event, component) {
  component.setValidity(false, event.target.validationMessage, false);
  event.preventDefault();
}

export default class FloatingLabel extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      message: '',
      valid: true,
      focus: false,
      active: !!props.value
    };
  }

  setValidity(valid, message = '', setNative = true) {
    this.setState({ valid, message });

    if (setNative) {
      if (!valid) {
        // use native system to block submit events.
        this.refs.input.setCustomValidity(message);
      } else {
        this.refs.input.setCustomValidity('');
      }
    }
  }

  render() {
    const { isTextarea, validator, ...props } = this.props;
    const id = this.props.id;
    const placeholder = this.props.placeholder;

    const className = classnames({
      'floatlabel-container': true,
      'floatlabel-invalid': !this.state.valid,
      'floatlabel-focus': this.state.focus,
      'floatlabel-active': this.state.active
    });

    const InputTag = isTextarea ? 'textarea' : 'input';

    return (
      <div className={ className }>
        <InputTag className="floatlabel-input"
          {...props}
          ref="input"
          onFocus={(e) => this.setState({ focus: true })}
          onBlur={(e) => this.setState({ focus: false })}
          onChange={(e) => _onChange(e.target, validator, this)}
          onInvalid={(e) => _onInvalid(e, this)}
        />
        <label className="floatlabel-label" htmlFor={id}>{placeholder}</label>
        <span className="floatlabel-message">{ this.state.message }</span>
      </div>
    );
  }
}

FloatingLabel.propTypes = {
  placeholder: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,

  /**
   * True if the input is a textarea
   */
  isTextarea: PropTypes.bool,

  /**
   * @param {!String} inputValue - The value of the input/textarea.
   * @return {!(String|boolean)} True if the input is valid, an error message otherwise.
   */
  validator: PropTypes.func
};

FloatingLabel.defaultProps = {
  isTextarea: false,
  validator: function () {
    return { valid: true, message: '' };
  }
};
