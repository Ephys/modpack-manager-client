import React, { PropTypes } from 'react';
import api from '../../core/api';
import FormUtil from '../utils/FormUtil';

function _hasCaptcha(children) {
  for (const child of children) {
    if (child.props.className && child.props.className.split(' ').includes('g-recaptcha')) {
      return true;
    }
  }

  return false;
}

function _isCaptchaValid(formComponent, data) {
  if (!_hasCaptcha(formComponent.props.children)) {
    return true;
  }

  const captchaResponse = data['g-recaptcha-response'];
  if (!captchaResponse) {
    return formComponent.stopRunning('Please validate your captcha.');
  }
}

function _onSubmit(event, formComponent, handlers) {
  event.preventDefault();

  if (formComponent.isRunning()) {
    return;
  }

  formComponent.startRunning();

  const formData = FormUtil.extractData(event.target);
  if (!_isCaptchaValid(formComponent, formData)) {
    return;
  }

  const submitHandler = handlers.onSubmit || api.call;
  const submitPromise = submitHandler(formComponent.props.action, formComponent.props.method || 'GET', formData);

  if (!submitPromise) {
    throw new Error('RestForm#onSubmit should return a promise.');
  }

  submitPromise.then(result => {
    const message = handlers.onSuccess && handlers.onSuccess(result);

    // page changing, this is dying we can't change the state.
    if (message !== false) {
      formComponent.stopRunning(message || '');
    }
  }).catch(e => {
    const message = (handlers.onError && handlers.onError(e)) || e.message || 'An error occured.';

    formComponent.stopRunning(message);
  });
}

export default class RestForm extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      running: false,
      message: ''
    };
  }

  startRunning() {
    this.setState({ running: true });
  }

  stopRunning(message = '') {
    this.setState({ running: false, message });
  }

  isRunning() {
    return this.state.running;
  }

  render() {
    const { onSuccess, onError, onSubmit, ...props } = this.props;

    let children = React.Children.map(this.props.children, child => {
      if (child.props.type === 'submit' && !child.props.disabled) {
        return React.cloneElement(child, {
          disabled: this.state.running
        });
      }

      return child;
    });

    return (
      <form { ...props } onSubmit={(e) => _onSubmit(e, this, { onSuccess, onError, onSubmit })}>
        { children }
        <span className="form-message">{ this.state.message }</span>
      </form>
    );
  }
}

RestForm.propTypes = {
  /**
   * Function called when an error occured during the execution of onSubmit.
   * @param error - The error that occured during onSubmit.
   * @return {string} A description of the error to print.
   */
  onError: PropTypes.func,

  /**
   * Function called after the form has been successfully submitted.
   * @param data - The data returned by the network call.
   * @return {string} A message to print.
   */
  onSuccess: PropTypes.func,

  /**
   * Function that handles the network call.
   * @param {!string} action - The form action prop.
   * @param {!string} method - The form method prop.
   * @param {!object} data - The list of named inputs with their associated values.
   * @return {!Promise} The promise handling the async network call.
   */
  onSubmit: PropTypes.func,

  action: PropTypes.string.isRequired,
  method: PropTypes.string
};
