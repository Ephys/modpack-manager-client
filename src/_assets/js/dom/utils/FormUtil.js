
export default {

  /**
   * Returns the values of the named inputs of a form.
   * @param {!HTMLFormElement} form - The form from which the data will be extracted.
   * @returns {!Object} The values, paired to the name of their input.
   */
  extractData(form) {
    const data = {};
    const inputs = form.elements;
    for (let i = 0; i < form.length; i++) {
      const name = inputs[i].name;

      if (name) {
        data[name] = inputs[i].value;
      }
    }

    return data;
  }
};
