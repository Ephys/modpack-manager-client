import React from 'react';
import { Link } from 'react-router';

export default React.createClass({
  render() {
    return (
      <div className="index-screen">
        Hi!
        <Link to="/logout">Log out</Link>
      </div>
    );
  }
});

export const path = '/';
