import React from 'react';
import { hashHistory } from 'react-router';
import zxcvbn from 'zxcvbn';

import Input from '../components/FloatingLabel';
import RestForm from '../components/RestForm';
import config from '../../core/config';
import api from '../../core/api';
import { path as indexPath } from './index';
import { ApiError } from '../../core/Error';

function _handleLogin(user) {
  api.setAuthToken(user.token);
  hashHistory.push(indexPath);

  return false;
}

function _handleError(error) {
  if (!(error instanceof ApiError)) {
    return;
  }

  switch (error.errorCode) {
    case 'ERR_INVALID_EMAIL':
      return 'Invalid email.';

    case 'ERR_INVALID_PASSWORD':
      return 'Invalid password.';

    case 'ERR_DUPLICATE_EMAIL':
      return 'Email already in use.';

    case 'ERR_WRONG_CAPTCHA':
      return 'Invalid captcha.';

    case 'ERR_MISSING_CAPTCHA':
      return 'Please validate the captcha.';
    default:
      return error.errorCode;
  }
}

function _validatePassword(password) {
  const estimation = zxcvbn(password);

  return {
    valid: estimation.score > 1, /* possibles levels are 0 through 4 */
    message: `It would take ${estimation.crack_times_display.online_no_throttling_10_per_second} to crack your password.`// jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
  };
}

export default React.createClass({
  render() {
    return (
      <div className="login-screen">
        <h1>Octopancake</h1>

        <RestForm onError={_handleError} onSuccess={_handleLogin} action="/user" method="post">
          <h2>Register</h2>
          <Input id="register-form-email" name="email" type="email" placeholder="Email" required />
          <Input id="register-form-password" name="password" type="password" placeholder="Password" required validator={_validatePassword} />

          <div data-sitekey={ config.RECAPTCHA_CLIENT_KEY } className="g-recaptcha"></div>

          <button type="submit">Register</button>
        </RestForm>

        <RestForm onError={_handleError} onSuccess={_handleLogin} action="/session" method="post">
          <h2>Sign in</h2>
          <Input id="login-form-email" name="email" type="email" placeholder="Email" required />
          <Input id="login-form-password" name="password" type="password" placeholder="Password" required />

          <button type="submit">Login</button>
        </RestForm>
      </div>
    );
  }
});

export const path = '/login';
